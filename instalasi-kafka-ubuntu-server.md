# Instalasi Kafka di Ubuntu Server #

1. Buat home folder untuk aplikasi kafka

    ```
    mkdir /var/lib/kafka
    ```

2. Buat user `kafka`

    ```
    useradd -d /var/lib/kafka -s /bin/bash kafka
    ```

3. Masuk ke folder kafka

    ```
    cd /var/lib/kafka
    ```

4. Download kafka

    ```
    wget -c "https://dlcdn.apache.org/kafka/3.2.0/kafka_2.13-3.2.0.tgz"
    ```

5. Extract

    ```
    tar xvzf kafka_2.13-3.2.0.tgz
    ```

6. Pindahkan hasil extract

    ```
    mv kafka_2.13-3.2.0/* ./
    ```

7. Hapus file dan folder yang sudah tidak dipakai

    ```
    rm -rf kafka_2.13-3.2.0*
    ```

8. Ganti kepemilikan file dan folder

    ```
    chown -R kafka:kafka /var/lib/kafka
    ```

9. Edit file `/var/lib/kafka/config/server.properties` untuk mengatur lokasi database kafka

    ```
    log.dirs=/var/lib/kafka/kafka-logs
    ```

10. Buat file `/etc/systemd/system/zookeeper.service` supaya Zookeeper berjalan sebagai system service. Isinya sebagai berikut

    ```
    [Unit]
    Requires=network.target remote-fs.target
    After=network.target remote-fs.target

    [Service]
    Type=simple
    User=kafka
    ExecStart=/var/lib/kafka/bin/zookeeper-server-start.sh /var/lib/kafka/config/zookeeper.properties
    ExecStop=/var/lib/kafka/bin/zookeeper-server-stop.sh
    Restart=on-abnormal

    [Install]
    WantedBy=multi-user.target
    ```

11. Buat file `/etc/systemd/system/kafka.service` supaya Kafka berjalan sebagai system service. Isinya sebagai berikut

    ```
    [Unit]
    Requires=zookeeper.service
    After=zookeeper.service

    [Service]
    Type=simple
    User=kafka
    ExecStart=/bin/sh -c '/var/lib/kafka/bin/kafka-server-start.sh /var/lib/kafka/config/server.properties > /var/lib/kafka/kafka.log 2>&1'
    ExecStop=/var/lib/kafka/bin/kafka-server-stop.sh
    Restart=on-abnormal

    [Install]
    WantedBy=multi-user.target
    ```

12. Enable service kafka supaya start pada saat boot

    ```
    systemctl enable zookeeper
    systemctl enable kafka
    ```

13. Test reboot untuk memastikan kafka jalan pada saat boot

14. Cek port `2181` dan `9092`

    ```
    netstat -plunt | grep 2181
    netstat -plunt | grep 9092
    ```