# Belajar Kafka #

* Arsitektur berbasis messaging

[![Arsitektur berbasis messaging](img/messaging-architecture.jpg)](img/messaging-architecture.jpg)

* Semantik messaging

[![Semantik messaging](img/messaging-semantic.jpg)](img/messaging-semantic.jpg)

* Arsitektur Kafka

[![Arsitektur Kafka](img/arsitektur-kafka.jpg)](img/arsitektur-kafka.jpg)

* Partisi & Offset

[![Partisi & Offset](img/partisi-dan-offset.jpg)](img/partisi-dan-offset.jpg)

* [Slide Presentasi Aplikasi Berbasis Messaging](https://docs.google.com/presentation/d/1fw4i_SO5W_EVYcE8fJm5epmxrGjJrjyCLzZ4f1XfvK4/edit#slide=id.g3997ace3e2_0_67)

* [Rekaman JVM Meetup tentang Messaging](https://www.youtube.com/watch?v=dBcRzxJQo4Q)

## Instalasi Kafka ##

Kebutuhan software :

* Java SDK 8 atau lebih tinggi

1. Unduh Kafka dari website resminya : [https://kafka.apache.org](https://kafka.apache.org)
2. Extract filenya
3. Masuk ke foldernya, dan langsung jalankan

Kafka membutuhkan Zookeeper untuk melakukan koordinasi dan sinkronisasi antar node. Zookeeper sudah dibundel bersama file installer Kafka.

## Menjalankan Kafka di Windows ##

1. Jalankan Zookeeper dulu

    ```
    .\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties
    ```

2. Jalankan Kafka

    ```
    .\bin\windows\kafka-server-start.bat .\config\server.properties
    ```

3. Cek port 

    ```
    netstat -am | find "2181"
    netstat -am | find "909"
    ```

## Menjalankan Kafka di Linux / Mac ##

1. Jalankan Zookeeper

    ```
    ./bin/zookeeper-server-start.sh ./config/zookeeper.properties
    ```

2. Jalankan Kafka

    ```
    ./bin/kafka-server-start.sh ./config/server.properties
    ```

3. Cek port `2181` dan `9092`

    * Mac

        ```
        netstat -anvp tcp | grep LISTEN | grep 2181
        netstat -anvp tcp | grep LISTEN | grep 9092
        ```

    * Linux

        ```
        netstat -plunt | grep 2181
        netstat -plunt | grep 909
        ```

## Membuat Multiple Broker ##

1. Copy `server.properties` ke `server-2.properties`

2. Ubah variabel berikut dalam file `server-2.properties`

    * `broker.id` set menjadi `1`
    * `listeners` di-uncomment, set menjadi `PLAINTEXT://:9093`
    * `log.dirs` set menjadi `/tmp/kafka-logs-2`

3. Start broker dengan konfigurasi yang baru

    ```
    ./bin/kafka-server-start.sh ./config/server-2.properties
    ```

4. Cek port zookeeper dan kafka. Pastikan port `2181`, `9092`, `9093` aktif

  * Mac

    ```
    netstat -anvp tcp | grep LISTEN | grep 2181
    netstat -anvp tcp | grep LISTEN | grep 909
    ```

# Instalasi Extension Kafka PHP #

1. Instal `rdkafka`. Panduannya bisa dilihat di [website rdkafka](https://github.com/edenhill/librdkafka)

    * MacOS : `brew install librdkafka`
    * Ubuntu : `apt install librdkafka-dev`

2. Instal extension php-rdkafka. Panduannya bisa dilihat [di websitenya](https://github.com/arnaud-lb/php-rdkafka)

    * MacOS
      
        ```
        brew tap shivammathur/extensions
        brew install shivammathur/extensions/rdkafka@8.1
        ```
    
    * Ubuntu

        ```
        apt install php8.1-dev php-pear
        sudo pecl install rdkafka
        ```

3. Instalasi library `laravel-kafka`. Panduannya bisa dilihat [di Githubnya](https://github.com/mateusjunges/laravel-kafka)

# Mengirim message ke Kafka #

Menggunakan library `php-rdkafka` secara langsung :

1. Konfigurasi lokasi kafka server

    ```php
    $conf = new RdKafka\Conf();
    $conf->set('log_level', (string) LOG_DEBUG);
    $conf->set('debug', 'all');
    $rk = new RdKafka\Producer($conf);
    $rk->addBrokers("localhost:9092");
    ```

2. Kirim data ke topic

    ```php
    $data = array(
        'client_id' => 'cl001',
        'product_id' => 'pln-postpaid',
        'customer_id' => '123456789012',
        'amount' => 12000,
        'tax' => 1200,
        'penalty' => 0,
        'response_code' => '00'
    );
    $topic = $rk->newTopic("inquiry-request");
    $topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($data, JSON_FORCE_OBJECT));
    $timeout_ms = 1000;
    $rk->flush($timeout_ms);
    ```

# Menerima message dari Kafka #

Problem :

* message bisa datang kapan saja
* script PHP biasanya baru dieksekusi pada waktu ada request HTTP

Keinginan :

* Penerimaan message dari kafka bisa dijalankan tanpa ada request HTTP, misalnya dipanggil langsung dari command line
* Menggunakan Laravel, sehingga bisa memanfaatkan fitur koneksi database, Eloquent ORM, dan fitur Laravel lainnya

Solusi :

* Menggunakan library [ArtisanDispatchable](https://freek.dev/1999-a-laravel-package-to-dispatch-jobs-via-artisan) agar Laravel Job bisa dipanggil dari command line.
* Menggunakan [Supervisord](https://supervisord.org/) untuk memantau dan merestart job bila mati. Panduannya bisa dibaca [di sini](https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps)

## Instalasi ArtisanDispatchable ##

Instalasi dilakukan dengan composer

```
composer install spatie/laravel-artisan-dispatchable
```

## Membuat Job KafkaListener ##

1. Membuat job

    ```
    php artisan make:job KafkaListener
    ```

2. Edit job, `implements ArtisanDispatchable` dan tulis isi method handle

   ```php
    <?php

    namespace App\Jobs;

    use Illuminate\Contracts\Queue\ShouldQueue;
    use Spatie\ArtisanDispatchable\Jobs\ArtisanDispatchable;
    use RdKafka;

    class KafkaListener implements ShouldQueue, ArtisanDispatchable
    {
        
        public function handle()
        {
            echo("Halo Kafka");

            $conf = new RdKafka\Conf();
            $conf->set('group.id', 'adapter-channel-2');
            $conf->set('metadata.broker.list', 'localhost:9092');
            $conf->set('auto.offset.reset', 'earliest');

            $consumer = new RdKafka\KafkaConsumer($conf);
            $consumer->subscribe(['inquiry-request']);
            while(true) {
                $message = $consumer->consume(120*1000);

                switch ($message->err) {
                    case RD_KAFKA_RESP_ERR_NO_ERROR:
                        var_dump($message->payload);
                        $consumer->commit();
                        break;
                    case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                        echo "No more messages; will wait for more\n";
                        break;
                    case RD_KAFKA_RESP_ERR__TIMED_OUT:
                        echo "Timed out\n";
                        break;
                    default:
                        throw new \Exception($message->errstr(), $message->err);
                        break;
                }
            }
        }
    }
    ```

3. Jalankan job dari command line

    ```
    php artisan kafka-listener
    ```

## Contoh Kasus ##

[![Contoh Kasus](img/contoh-aplikasi.jpg)](img/contoh-aplikasi.jpg)

[![Contoh Kasus Notifikasi](img/contoh-aplikasi-notifikasi.jpg)](img/contoh-aplikasi-notifikasi.jpg)

## Event Sourcing ##

[![Event Sourcing](img/event-sourcing.jpg)](img/event-sourcing.jpg)

## Monitoring Kafka dengan Prometheus dan Grafana ##

* [https://computingforgeeks.com/monitor-apache-kafka-with-prometheus-and-grafana/](https://computingforgeeks.com/monitor-apache-kafka-with-prometheus-and-grafana/)
* [https://grafana.com/solutions/kafka/monitor/](https://grafana.com/solutions/kafka/monitor/)