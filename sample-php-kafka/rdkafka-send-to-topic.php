<?php

use RdKafka;

$data = array(
    'client_id' => 'cl001',
    'product_id' => 'pln-postpaid',
    'customer_id' => '123456789012',
    'amount' => 12000,
    'tax' => 1200,
    'penalty' => 0,
    'response_code' => '00'
);

// konfigurasi broker
$conf = new RdKafka\Conf();
$conf->set('log_level', (string) LOG_DEBUG);
$conf->set('debug', 'all');
$rk = new RdKafka\Producer($conf);
$rk->addBrokers("localhost:9092");

// kirim data ke topic
$topic = $rk->newTopic("inquiry-request");
$topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($data, JSON_FORCE_OBJECT));

$timeout_ms = 1000;
$rk->flush($timeout_ms);