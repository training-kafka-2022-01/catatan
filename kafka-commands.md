# Perintah Command Line Kafka #


1. Melihat daftar topic dalam server

    ```
    bin/kafka-topics.sh --bootstrap-server localhost:9092 --list
    ```

2. Membuat topic `inquiry-response`

    ```
    bin/kafka-topics.sh --bootstrap-server localhost:9092 --create --topic inquiry-response --partitions 1 --replication-factor 1
    ```

3. Menghapus topic `coba`

    ```
    bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic coba
    ```

4. Melihat informasi detail tentang topic `inquiry-request`

    ```
    bin/kafka-topics.sh --bootstrap-server localhost:9092 --describe --topic inquiry-request
    ```

5. Melihat informasi `consumer` dalam topic `inquiry-request`

    ```
    bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --list
    ```

6. Melihat informasi detail tentang consumer `adapter-channel`

    ```
    bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group adapter-channel
    ```

